#IMPORTS 
from albumentations.augmentations.crops.transforms import CenterCrop
import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import json
from random_preprocessing.img_preprocessing import ratios
import albumentations as A
import cv2

#------------------------------------------------------------------------------------------------------------------------
# PARAMETERS
_IMAGES_PATH_ = 'segmentation_experiments/panoptic' #INSERT CORRECT PATH ACCORDINGLY
_FLAG_ = 'validation'
desired_size = 224
# Flag represents your set. Change accordingly
flag = 'validation' # duplicate code, but whatever...
df = pd.read_csv(f'./{flag}.csv') # read the corresponding DataFrame, created by img_preprocessing.py
transform = A.Compose([
    A.Resize(width = desired_size, height = desired_size), 
    #A.HorizontalFlip(p=0.0),
    #A.RandomBrightnessContrast(p=0.0),
], bbox_params=A.BboxParams(format='coco'))

#------------------------------------------------------------------------------------------------------------------------
#FUNCTION to modify json dimensions based on the preprocessed ones (without padding)
def json_mod(data, flagging):

     with open('panoptic_2018_copy.json',"r") as json_file: # open the json file
          images_json = json.load(json_file)

     for i in range(len(images_json['images'])): # json iteration (image as index)
          for j in range(len(data)): # DataFrame iteration (image as index)
               if data.iloc[j][0] in images_json['images'][i]['file_name']: # if they match, modify json dimensions in accordance to that of the DataFrame
                         images_json['images'][i]['width'] = int(data.iloc[j][1])
                         images_json['images'][i]['height'] = int(data.iloc[j][2])
                         #data = data.drop([j]) # reduce size of DataFrame

     # Writing to panoptic_modified_{flag}.json
     with open(f'panoptic_2018_modified_dimensions_{flagging}.json', 'w') as outfile:
          json.dump(images_json, outfile)

#------------------------------------------------------------------------------------------------------------------------
#FUNCTION to modify json dimensions based on the ratios, calculated in real time 
def json_mod_with_ratio():
     with open('panoptic_2018_copy.json',"r") as json_file: # open the json file
          images_json = json.load(json_file)

     for i in range(len(images_json['images'])): # json iteration (image as index)
          w, h = images_json['images'][i]['width'], images_json['images'][i]['height']
          _, n_size, _, _, _, _, _, _ = ratios(w, h, desired_size) # return new dimensions
          images_json['images'][i]['width'] = int(n_size[1])
          images_json['images'][i]['height'] = int(n_size[0])

     # Writing to panoptic_modified_{flag}.json
     with open(f'panoptic_2018_modified_dimensions_{flag}.json', 'w') as outfile:
          json.dump(images_json, outfile)

#------------------------------------------------------------------------------------------------------------------------
#FUNCTION to extract bboxes
def bbx():
     bboxes = []
     names = []

     with open('panoptic_2018_copy.json',"r") as json_file: # open the json file
          images_json = json.load(json_file)

     for i in range(len(images_json['annotations'])): # json iteration (annotations as index)
          sub_bboxes = []
          names.append(images_json['annotations'][i]['file_name'])
          for j in range(len(images_json['annotations'][i]['segments_info'])): # json iteration (segments as index)
               box = images_json['annotations'][i]['segments_info'][j]['bbox']
               box.append(images_json['annotations'][i]['segments_info'][j]['category_id'])
               sub_bboxes.append(box)
               #sub_bboxes.append(images_json['annotations'][i]['segments_info'][j]['bbox'])
               #sub_bboxes.append(images_json['annotations'][i]['segments_info'][j]['category_id'])
          bboxes.append(sub_bboxes)

     return bboxes, names

#------------------------------------------------------------------------------------------------------------------------
#FUNCTION to modify json BBOX dimensions
def bbox_json(boxes, IDs):

     with open('panoptic_2018_copy.json','r') as json_file:
          images_json = json.load(json_file)

     for i in range(len(images_json['annotations'])):
          filename = images_json['annotations'][i]['file_name']
          image = cv2.imread(f'{_IMAGES_PATH_}/{filename}')
          if image is not None:
               mod_set = transform(image = image, bboxes = boxes[IDs.index(filename)])
               new_boxes = mod_set['bboxes']
               new_boxes = list(map(list, new_boxes))

               for idx, element in enumerate(new_boxes):
                    element.pop()
                    element = [round(elem, 0) for elem in element]
                    images_json['annotations'][i]['segments_info'][idx]['bbox'] = element
          else: continue
          
     with open(f'panoptic_2018_bboxes_modified_{_FLAG_}.json', 'w') as outfile:
          json.dump(images_json, outfile)

#------------------------------------------------------------------------------------------------------------------------
def show_img(img):
     plt.imshow(img)
     plt.show()

#------------------------------------------------------------------------------------------------------------------------
#json_mod(df, flag)
#json_mod_with_ratio()
bboxes, names = bbx()
bbox_json(bboxes, names)
