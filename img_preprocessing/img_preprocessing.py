#IMPORTS 
import cv2
import numpy as np
import os
import pandas as pd

#PARAMETERS
desired_size = 224
padding_color = [255, 255, 255]# WHITE PADDING
flag = 'validation' # change according to your folder (set) name (training/validation/test)
_IMAGES_PATH_ = f'img_preprocessing/{flag}/images'
_IMAGES_NAMES_ = os.listdir(_IMAGES_PATH_)

#FUNCTION TO PREPROCESS 
def preprocessor(image_name):

    img_read = cv2.imread(f'{_IMAGES_PATH_}/{image_name}')
    old_size = img_read.shape[:2] # size is in HEIGHT x WIDTH format

    if old_size[1] > old_size[0]: # width > height
        ratio = int(desired_size * (old_size[0]/old_size[1])) # height / width
        new_size = tuple([ratio, desired_size]) # define the new size based on expected one

        #width_padding = desired_size - new_size[1]# define total width padding
        height_padding = desired_size - new_size[0]# define total height padding
        top, bottom = 0, height_padding//2 + height_padding-(height_padding//2)
        #left, right = 0, 0

        # new_size should be in (width, height) format
        new_img = cv2.resize(img_read, (new_size[1], new_size[0]))

        img_final = cv2.copyMakeBorder(new_img, top, bottom, 0, 0, cv2.BORDER_CONSTANT, value = padding_color)

    else: # height > width
        ratio = int(desired_size * (old_size[1]/old_size[0])) # width / height
        new_size = tuple([desired_size, ratio]) # define the new size based on expected one

        width_padding = desired_size - new_size[1]# define total width padding
        #height_padding = desired_size - new_size[0]# define total height padding
        #top, bottom = 0, 0
        left, right = 0, width_padding//2 + width_padding-(width_padding//2)
        
        # new_size should be in (width, height) format
        new_img = cv2.resize(img_read, (new_size[1], new_size[0]))
        
        img_final = cv2.copyMakeBorder(new_img, 0, 0, left, right, cv2.BORDER_CONSTANT, value = padding_color)
        
    new_list_row = [image_name, new_size[1], new_size[0]] 

    return img_final, new_list_row

#FUNCTION TO READ, SEND FOR PROCESSING
def image_reader():
    img_info = []

    for image_name in _IMAGES_NAMES_:
        final_image, row = preprocessor(image_name) # get image
        img_info.append(row) # append img info to list
        
        # image_writer(image_name, final_image) #CALL WRITER (OR NOT)

    return img_info

#FUNCTION TO WRITE
def image_writer(name, final):

        # Save .jpg image
        cv2.imwrite(f'img_preprocessing/validation_tests/preprocessed/{name}', final, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        print(f'file with name {name} has been preprocessed and saved!')

info_final = image_reader()

df = pd.DataFrame(info_final, columns = ['name', 'width', 'height']) # create datafrane
df.to_csv(f'./{flag}.csv', index_label = False) # save datafrane locally

"""
cv2.imshow("image", img_final)
cv2.waitKey(0)
cv2.destroyAllWindows()
"""